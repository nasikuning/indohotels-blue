<?php
/* Template Name: Rooms Page Template */ get_header('image'); ?>

<main role="main">
	<div class="container text-center">
		<!-- container -->
		<!-- section -->
		<section>
			<h1 class="title text-center">
				<?php the_title(); ?>
			</h1>
		</section>
		<section>
			<?php
			$args = array('post_type'=>'rooms');
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="box-container col-md-12">
				<div class="room-thumb thumbnail">
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<div class="row">
							<div class="col-md-6">
								<!-- post thumbnail -->
								<div class="thumb">
									<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail(array(300,150)); // Declare pixel size you need inside the array ?>
									</a>
									<?php endif; ?>
								</div>
								<!-- /post thumbnail -->
							</div>
							<div class="col-md-6">
								<div class="box-text">
									<!-- post title -->
									<h2 class="title-room-list">
										<?php the_title(); ?>
									</h2>
									<!-- /post title -->

									<div class="room-info">
										<?php echo rwmb_meta('indohotels_room_balcony'); ?>
									</div>
									<?php the_excerpt(); ?>
									<!-- <button class="book-room btn"><a href="<?php //the_permalink(); ?>">Room Details</a></button>
-->
								</div>
							</div>
						</div>
					</article>
					<!-- /article -->
				</div>
			</div>

			<?php endwhile; ?>

			<?php else: ?>

			<!-- article -->
			<article>
				<h2>
					<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
				</h2>
			</article>
			<!-- /article -->

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>
	<!-- end container -->
</main>

<?php get_footer(); ?>
