<?php get_header(); ?>

<main role="main">
	<!-- section -->
	<section class="container">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="box-book-rooms"><!-- box booking details -->
					<div class="room-details">
						<div class="col-sm-12 col-md-12">
							<div class="room-booking">
								<div class="room-box">
									<div class="room-title-box text-center">
										<h2 class="room-title"><?php the_title(); ?></h2>
										<div class="room-balcony">
											<?php echo rwmb_meta('indohotels_room_balcony'); ?>
										</div>
									</div>
									<!-- Slider -->
									<div class="span12" id="slider">
										<!-- Top part of the slider -->
										<div class="span8" id="carousel-bounding-box">
											<div class="carousel slide" id="myCarousel">
												<!-- Carousel items -->
												<div class="carousel-inner">
													<?php
													$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
													if ( !empty( $images ) ) {
														$i = 0;
														foreach ( $images as $image ) {
															if($i++ == 0) {
																$active = 'active';
															} else {$active = '';}
															echo '<div class="'. $active .' item">';
															echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
															echo '</div>';
														}
													}
													?>
												</div>
												<!-- Carousel nav -->
												<a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>
											</div>
										</div>
									</div><!--/Slider-->

									<div class="room-details-spec col-md-12">
										<span class="col-md-3"><span class="room-value">Room Size :</span> <?php echo rwmb_meta( 'room_size' ); ?> </span>
										<span class="col-md-3"><span class="room-value">View : </span><?php echo rwmb_meta( 'room_view' ); ?></span>
										<span class="col-md-3"><span class="room-value">Ocupancy :</span> <?php echo rwmb_meta( 'room_occupancy' ); ?>  Person</span>
										<span class="col-md-3"><span class="room-value">Bed Size :</span> <?php echo rwmb_meta( 'bed_size' ); ?></span>
									</div>
								</div>

								<div class="room-details-desc">
									<?php the_content(); ?>
								</div>

								<h3 class="room-details-f-title"><?php echo "Room features"; ?></h3>
								<div class="feature-box col-md-12">

									<div class="col-md-4">
										<h4>For Your Convenience</h4>
										<ul class="room-features">
											<?php
											$values = rwmb_meta( 'room_convenience' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>

										</ul>
									</div>
									<div class="col-md-4">
										<h4>For Your Indulgence</h4>
										<ul class="room-features">
											<?php
											$values = rwmb_meta( 'room_indulgence' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</div>
									<div class="col-md-4">
										<h4>For Your Comfort</h4>
										<ul class="room-features">
											<?php
											$values = rwmb_meta( 'room_comfort' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</div>
								</div>


								<div class="text-center">
									<button type="button" class="btn btn-check">Check Availability</button>
								</div>

							</div>
						</div>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

	<?php get_footer(); ?>
