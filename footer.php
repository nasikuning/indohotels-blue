			<!-- footer -->
			<footer style="background: <?php echo ot_get_option( 'krs_main_colorpicker'); ?>" class="footer">
				<div class="footer-credits">
					<?php echo ot_get_option('krs_footcredits'); ?>

				</div>

				<?php krs_sn(); ?>
				<nav class="nav text-center nav-bottom">
					<?php karisma_nav_footer(); ?>
				</nav> <!-- End nav -->

			</footer>

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

	</body>
	</html>
