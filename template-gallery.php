<?php /* Template Name: Gallery Template */ get_header('image'); ?>

<main role="main">
	<!-- section -->
	<section class="container">
		<?php
		$args = array('post_type'=>'gallery');
		query_posts($args);
		?>
		<h1 class="title text-center"><?php the_title(); ?></h1>
		<div class="container">
			<div id="main_area" class="col-md-12 box-gallery">
				<!-- Slider -->
					<div class="span12" id="slider">
						<!-- Top part of the slider -->
							<div class="span8" id="carousel-bounding-box">
								<div class="carousel slide" id="myCarousel">
									<!-- Carousel items -->
									<div class="carousel-inner">
										<?php if (have_posts()):
										$i=0;
										while (have_posts()) : the_post();
										?>
										<div class="<?php echo ($i==0)?'active':'' ?> item" data-slide-number="<?php echo $i; ?>">
											<?php the_post_thumbnail('gallery-slide');?>
										</div>
										<?php $i++; ?>
									<?php endwhile; ?>
								<?php endif; ?>

							</div><!-- Carousel nav -->
							<a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>
						</div>
					</div>


			</div>
		<!--/Slider-->

		<div class="row hidden-phone" id="slider-thumbs">
			<div class="col-md-12">
				<!-- Bottom switcher of slider -->
				<div class="thumbnail-box">
					<div class="carousel-gallery owl-carousel">
						<?php if (have_posts()):
						$i=0;
						while (have_posts()) : the_post();
						?>
						<div class="<?php echo ($i==0)?'active':'' ?> item">
							<a class="thumbnail" id="carousel-selector-<?php echo $i; ?>"><?php the_post_thumbnail('medium');?></a>
						</div>
						<?php $i++; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

</section>
<!-- /section -->
</main>

<?php //get_sidebar(); ?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		/* Testimonial */
		$('.carousel-gallery').owlCarousel({
			margin:5,
			autoplay:true,
			autoplayTimeout:7000,
			autoplayHoverPause:true,
			loop:false,
			nav:true,
			navText: ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'>"],
			responsiveClass:true,
			responsive:{
				0:{
					items:10,
					margin:5,
				},
				600:{
					items:10,
					margin:5,
				},
				1024:{
					items:10,
					margin:5,
				}
			}
		});
	});
</script>
<?php get_footer(); ?>
