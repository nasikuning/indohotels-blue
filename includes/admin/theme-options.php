<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', '_custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function _custom_theme_options() {

  /* OptionTree is not loaded yet, or this is not an admin request 
  * if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
   * return false;
    */
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'content'       => array( 
        array(
          'id'        => 'option_types_help',
          'title'     => __( 'Option Types', 'karisma_text_domain' ),
          'content'   => __( '<p>Help content goes here!</p>', 'karisma_text_domain' )
          )
        ),
      'sidebar'       => __( '<p>Sidebar content goes here!</p>', 'karisma_text_domain' )
      ),
    'sections'        => array( 
      array(
        'id'          => 'general_tab',
        'title'       => __( 'General', 'karisma_text_domain' )
        ),
      array(
        'id'          => 'homepage_tab',
        'title'       => __( 'Homepage', 'karisma_text_domain' )
        ),
      array(
        'id'          => 'custom_tab',
        'title'       => __( 'Custom Web', 'karisma_text_domain')
        ),
      array(
        'id'          => 'social_tab',
        'title'       => __( 'Social Network', 'karisma_text_domain' )
        ),
      array(
        'id'          => 'comment_tab',
        'title'       => __( 'Comments', 'karisma_text_domain' )
        ),
      array(
        'id'          => 'license_tab',
        'title'       => __( 'Theme License', 'karisma_text_domain' )
        ),
      array(
        'id'          => 'themeinfo_tab',
        'title'       => __( 'System Information', 'karisma_text_domain' )
        ),
      ),
    'settings'        => array( 
      array(
        'id'          => 'krs_logo_actived',
        'label'       => __( 'Enable logo image?', 'karisma_text_domain' ),
        'desc'        => __( 'Please check yes for enable logo image and check no for disable logo image.', 'karisma_text_domain' ),
        'std'         => 'yes',
        'type'        => 'radio',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'yes',
            'label'       => __( 'Yes', 'karisma_text_domain' ),
            'src'         => ''
            ),
          array(
            'value'       => 'no',
            'label'       => __( 'No', 'karisma_text_domain' ),
            'src'         => ''
            )
          )
        ),
      array(
        'id'          => 'krs_logo',
        'label'       => __( 'Your logo image', 'karisma_text_domain' ),
        'desc'        => __( 'Upload your logo image and type full path here. Default width:232px and height:90px.', 'karisma_text_domain' ),
        'std'         => krs_url .'asset/images/logo.svg',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'krs_logo_actived:is(yes),krs_logo_actived:not()',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_favicon',
        'label'       => __( 'Your Favicon', 'karisma_text_domain' ),
        'desc'        => __( 'Upload your ico image or type full path here.', 'karisma_text_domain' ),
        'std'         => krs_url .'asset/images/favicon.ico',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_appleicon',
        'label'       => __( 'Your Apple Touch Icon', 'karisma_text_domain' ),
        'desc'        => __( 'Upload a 16 x 16 px image that will represent your website\'s favicon. You can refer to this link for more information on how to make it: http://www.favicon.cc/', 'karisma_text_domain' ),
        'std'         => krs_url .'asset/images/touch.png',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_head',
        'label'       => __( 'Head Code', 'karisma_text_domain' ),
        'desc'        => __( 'Enter the code which you need to place before closing tag. (ex: Google Webmaster Tools verification, Bing Webmaster Center, BuySellAds Script, Alexa verification etc.)', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_footer',
        'label'       => __( 'Footer Script', 'karisma_text_domain' ),
        'desc'        => __( 'If you need to add scripts to your footer, do so here. (ex: Google Analytics).', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_footcredits',
        'label'       => __( 'Text footer credits', 'karisma_text_domain' ),
        'desc'        => __( 'You can change or remove karisma link from footer and use your own custom text. Tip: You can use our affiliate link and earn 60% commission on every sale.', 'karisma_text_domain' ),
        'std'         => '© 2017 Powered by <a href="//indohotels.id">PT. Indohotels Booking Systems</a>',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      /* Homepage */
      array(
        'id'          => 'krs_gallery',
        'label'       => __( 'Home Slider', 'karisma_text_domain' ),
        'desc'        => __( 'You can upload your photos for home slideshow.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'gallery',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_ros_in_title',
        'label'       => __( 'Wellcome Title', 'karisma_text_domain' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_tab',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_ros_in',
        'label'       => __( 'Wellcome Text', 'karisma_text_domain' ),
        'desc'        => __( 'Type your Ros in Hotel.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'gallery-slideshow',
        'label'       => __( 'Enable slideshow at middle?', 'karisma_text_domain' ),
        'desc'        => __( 'If you want enable this please go to <a href="http://hotel.indohotels.id/website-gold/wp-admin/edit.php?post_type=gallery" target="_blank">Add Gallery Post</a> with "home" post type.', 'karisma_text_domain' ),
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'homepage_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'on',
            'label'       => __( 'On', 'karisma_text_domain' ),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __( 'Off', 'karisma_text_domain' ),
            'src'         => ''
            )
          )
        ),
      /* Custom Website */
      array(
        'id'          => 'krs_main_colorpicker',
        'label'       => __( 'Change main color', 'karisma_text_domain' ),
        'desc'        => __( 'You can change main color website.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'custom_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_secound_colorpicker',
        'label'       => __( 'Change secoundary color', 'karisma_text_domain' ),
        'desc'        => __( 'You can change secoundary color website.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'custom_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_tertiary_colorpicker',
        'label'       => __( 'Change tertiary color', 'karisma_text_domain' ),
        'desc'        => __( 'You can change tertiary color website.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'custom_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_main_fonts',
        'label'       => __( 'Change Body typography', 'karisma_text_domain' ),
        'desc'        => __( 'You can change main typography themes.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'custom_tab',
        'rows'        => '3',
        'allowed_fields' => array('font-family','font-size','font-color'),
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_lead_fonts',
        'label'       => __( 'Change Lead typography', 'karisma_text_domain' ),
        'desc'        => __( 'You can change lead typography such as H1, H2, H3 text.', 'karisma_text_domain' ),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'custom_tab',
        'rows'        => '3',
        'allowed_fields' => array('font-family','font-size','font-color'),
        'operator'    => 'and'
        ),

/* 
  * Karisma Social network
  */
array(
  'label'       => __( 'Enable footer social right footer?', 'karisma_text_domain' ),
  'id'          => 'krs_head_social_activated',
  'type'        => 'radio',
  'desc'        => __( 'Please check yes for enable footer social and check no for disable footer social.', 'karisma_text_domain' ),
  'choices'     => array(
    array (
      'label'       => 'Yes',
      'value'       => 'yes'
      ),
    array (
      'label'       => 'No',
      'value'       => 'no'
      )
    ),
  'std'         => 'yes',
  'section'     => 'social_tab'
  ),

array(
  'label'       => __( 'Enable social shared post?', 'karisma_text_domain' ),
  'id'          => 'krs_active_shared',
  'type'        => 'radio',
  'desc'        => __( 'Please check yes for enable social shared post and check no for disable shared post.', 'karisma_text_domain' ),
  'choices'     => array(
    array (
      'label'       => 'Yes',
      'value'       => 'yes'
      ),
    array (
      'label'       => 'No',
      'value'       => 'no'
      )
    ),
  'std'         => 'yes',
  'section'     => 'social_tab'
  ),  
array(
  'label'       => __( 'Facebook application id', 'karisma_text_domain' ),
  'id'          => 'krs_facebook_app_id',
  'type'        => 'text',
  'desc'       => __( 'Fill with your facebook application id. To add a Facebook Comments Box you will need to create a Facebook App first. Go to https://developers.facebook.com/apps/ and create a new app. Enter a name and define the locale for the app.', 'karisma_text_domain' ),
  'std'         => '231372170246816',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Facebook URL', 'karisma_text_domain' ),
  'id'          => 'krs_fb_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your facebook url here, for example https://facebook.com/amrikarisma(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Twitter URL', 'karisma_text_domain' ),
  'id'          => 'krs_tweet_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your twitter url here, for example https://twitter.com/amrikarisma(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Gplus URL', 'karisma_text_domain' ),
  'id'          => 'krs_gplus_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your google plus url here. For example http://plus.google.com/3894293874219340 (use http://)', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Linkedin URL', 'karisma_text_domain' ),
  'id'          => 'krs_in_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your linkedin url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Dribble URL', 'karisma_text_domain' ),
  'id'          => 'krs_dribble_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your dribble url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Flickr URL', 'karisma_text_domain' ),
  'id'          => 'krs_flickr_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your flickr url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Instagram URL', 'karisma_text_domain' ),
  'id'          => 'krs_instagram_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your Instagram url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Tumblr URL', 'karisma_text_domain' ),
  'id'          => 'krs_tumblr_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your tumblr url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'Youtube URL', 'karisma_text_domain' ),
  'id'          => 'krs_youtube_sn',
  'type'        => 'text',
  'desc'       => __( 'Please enter your youtube url here(use http://).', 'karisma_text_domain' ),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __( 'RSS', 'karisma_text_domain' ),
  'id'          => 'krs_rss_sn',
  'type'        => 'radio',
  'desc'        => __( 'Please check yes for enable RSS link and check no for disable RSS link.', 'karisma_text_domain' ),
  'choices'     => array(
    array (
      'label'       => 'Yes',
      'value'       => 'yes'
      ),
    array (
      'label'       => 'No',
      'value'       => 'no'
      )
    ),
  'std'         => 'yes',
  'section'     => 'social_tab'
  ),
  /* 
  * Karisma comment
  */
  array(
    'label'       => __( 'Enable Facebook comment?', 'karisma_text_domain' ),
    'id'          => 'krs_facebook_com_activated',
    'type'        => 'radio',
    'desc'        => __( 'Please check yes for enable Facebook comment and check no for disable Facebook comment.', 'karisma_text_domain' ),
    'choices'     => array(
      array (
        'label'       => 'Yes',
        'value'       => 'yes'
        ),
      array (
        'label'       => 'No',
        'value'       => 'no'
        )
      ),
    'std'         => 'yes',
    'section'     => 'comment_tab'
    ),
  array(
    'label'       => __( 'Enable Default comment?', 'karisma_text_domain' ),
    'id'          => 'krs_def_com_activated',
    'type'        => 'radio',
    'desc'        => __( 'Please check yes for enable Default comment and check no for disable Default comment.', 'karisma_text_domain' ),
    'choices'     => array(
      array (
        'label'       => 'Yes',
        'value'       => 'yes'
        ),
      array (
        'label'       => 'No',
        'value'       => 'no'
        )
      ),
    'std'         => 'yes',
    'section'     => 'comment_tab'
    ),
  /* theme info */
  array(
    'label'       => __( 'System information', 'karisma_text_domain' ),
    'id'          => 'krs_textblock',
    'type'        => 'textblock',
    'desc'        => krs_themes_info(),
    'section'     => 'themeinfo_tab'
    ),
  array(
    'label'       => __( 'Theme License', 'karisma_text_domain' ),
    'id'          => 'krs_textlicense',
    'type'        => 'textblock',
    'desc'        => krs_license(),
    'section'     => 'license_tab'
    ),
  )
);

/* allow settings to be filtered before saving */
$custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );

/* settings are not the same update the DB */
if ( $saved_settings !== $custom_settings ) {
  update_option( 'option_tree_settings', $custom_settings ); 
}

}