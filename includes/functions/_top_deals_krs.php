<?php function top_deals(){ ?>
<section class="deals">
	<div class="box-container">

		<?php 
		$args = array('post_type'=>'deals');
		query_posts($args);
		if (have_posts()): while (have_posts()) : the_post(); ?>
		<div class="col-md-4">
			<div class="room-thumb thumbnail">
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class('deals-post'); ?>>
					<!-- post title -->
					<h2 class="title-room-list">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h2>
					<!-- /post title -->
					<!-- post thumbnail -->
					<div class="thumb">
						<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a href="javacript:void(0);" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(array(300,150)); // Declare pixel size you need inside the array ?>
							</a>
						<?php endif; ?>
					</div>
					<!-- /post thumbnail -->

				</article>
				<!-- /article -->
			</div>
		</div>

	<?php endwhile; ?>
<?php endif; ?>
</div>

<div class="clearfix"></div>
</section>
<?php }