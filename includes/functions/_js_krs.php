<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }


// Load karisma scripts (header.php)
function krs_loadjs()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {


        wp_register_script('bootstrap', krs_style . 'vendor/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7'); // Bootstrap scripts
        wp_register_script( 'animated-js',krs_style . 'js/animated.js',array('jquery'), '0.0.2' );
        wp_register_script( 'viewportchecker',krs_style . 'js/lib/viewportchecker.js',array('jquery'), false, true );
        wp_register_script('main-js', krs_style . 'js/main.js', array('jquery'), '0.0.2' );
        wp_register_script('owl-js', krs_style . 'vendor/owl/owl.carousel.min.js', array(), false, true );
        wp_register_script('slick-js', krs_style . 'vendor/slick/slick.min.js', array(), false, true );

        wp_enqueue_script('bootstrap'); // Enqueue it!
        wp_enqueue_script('viewportchecker'); // Enqueue it!
        wp_enqueue_script('animated-js'); // Enqueue it!
        wp_enqueue_script('main-js' ); // load all custom javascript
        wp_enqueue_script('owl-js');
            wp_enqueue_script('slick-js');
    }
}
