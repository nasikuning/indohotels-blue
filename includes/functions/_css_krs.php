<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

// Load karisma styles
function krs_styles()
{
    wp_register_style('karisma-style', krs_url . 'style.css', array(), '0.1.5', 'all');
    wp_register_style('bootstrap-min', krs_style . 'vendor/bootstrap/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_register_style('font-awesome-min', krs_style . 'vendor/font-awesome/css/font-awesome.min.css', array(), '4.7.0', 'all');
    wp_register_style('animate-css', krs_style . 'vendor/animate/css/animate.css', array(), '0.7.0', 'all');
    wp_register_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '1.0', 'all');
    wp_register_style('open-sans','//fonts.googleapis.com/css?family=Open+Sans:400,600,700,800', array(), '1.0','all' );
    wp_register_style('raleway','//fonts.googleapis.com/css?family=Raleway:300,400,500,600', array(), '1.0','all' );
    wp_register_style('caleran', krs_style . 'vendor/caleran/css/caleran.min.css', array(), '1.2','all' );
    wp_register_style('owl', krs_style . 'vendor/owl/owl.carousel.min.css', array(), '1.2','all' );
    wp_register_style('owl-theme', krs_style . 'vendor/owl/owl.theme.default.min.css', array(), '1.2','all' );
    wp_register_style('slick', krs_style . 'vendor/slick/slick.css', array(), '1.2','all' );
    wp_register_style('slick-theme', krs_style . 'vendor/slick/slick-theme.css', array(), '1.2','all' );

    wp_enqueue_style('jquery-ui'); // Enqueue bootstrap.min.css!
    wp_enqueue_style('bootstrap-min'); // Enqueue bootstrap.min.css!
    wp_enqueue_style('animate-css'); // Enqueue animate.css!
    wp_enqueue_style('font-awesome-min'); // Enqueue font-awesome.min.css!
    wp_enqueue_style('open-sans');
    wp_enqueue_style('raleway');
    wp_enqueue_style('owl' );
    wp_enqueue_style('owl-theme' );
    wp_enqueue_style('caleran');
    if (is_page('gallery')) :
        wp_enqueue_style('slick');
    wp_enqueue_style('slick-theme');
    endif;
    wp_enqueue_style('karisma-style'); // Enqueue Style.css!
    }
