<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/* 
* change text to leave a reply on comment form
*/
function krs_comment_text ($arg) {
	$arg['title_reply'] = __('Leave a reply', 'karisma_text_domain') .' "' . get_the_title() . '"';
	return $arg;
}

/*
* Override default new comment_form() with own
* better way to override comment form
*/
function krs_com_fields($fields) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$fields['author'] = '<div class="wrap"><div class="col-md-4"><div class="form-group has-feedback"><label class="control-label sr-only" for="author">Name</label><input type="text" name="author" class="form-control btn-box" id="form-control" placeholder="' . __( 'Name', 'karisma_text_domain' ) .' ' . ( $req ? __( '*', 'karisma_text_domain' ) : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" tabindex="1" ' . $aria_req . ' /><span class="glyphicon glyphicon-user form-control-feedback"></span></div></div>';
	$fields['email'] = '<div class="col-md-4"><div class="form-group has-feedback"><label class="control-label sr-only" for="email">Email</label><input type="text" name="email" class="form-control btn-box" id="form-control" placeholder="' . __( 'Email', 'karisma_text_domain' ) .' ' . ( $req ? __( '*', 'karisma_text_domain' ) : '' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" tabindex="2" ' . $aria_req . ' /><span class="glyphicon glyphicon-envelope form-control-feedback"></span></div></div>';
	$fields['url'] =  '<div class="col-md-4"><div class="form-group has-feedback"><label class="control-label sr-only" for="url">URL</label><input type="text" name="url" class="form-control btn-box" id="form-control" placeholder="' . __( 'Website', 'karisma_text_domain' ) .' ' . ( $req ? __( '*', 'karisma_text_domain' ) : '' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" tabindex="3" ' . $aria_req . ' /><span class="glyphicon glyphicon-globe form-control-feedback"></span></div></div></div>';
	return $fields;
}

function krs_com_fields_textarea($fields_com) {
	$fields_com = '<div class="textarea-form"><textarea name="comment" cols="100%" rows="5" class="form-control btn-box" tabindex="4"></textarea></div>';
	return $fields_com;
}

/* 
* Add comment list
* You can find comment navigation function in comments.php
* Call via callback wp_list_comments( array( 'callback' => 'krs_comments' ) ); in comments.php
*/
if ( ! function_exists( 'krs_comments' ) ) :
	function krs_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
			<div class="commentwrapper clearfix <?php $author_id = get_the_author_meta('ID'); if($author_id == $comment->user_id) $author_flag = 'true';?>" id="comment-<?php comment_ID(); ?>">
				<div class="author-card pull-left clearfix">
					<?php echo get_avatar( $comment, $size='52', '', $alt='author' ); ?>
				</div>
				<div class="comment_data">
					<span class="fa fa-caret-left"></span>
					<?php if (isset($author_flag) && ($author_flag == 'true')) { ?><span class="author_comment"><?php _e( 'Author','karisma_text_domain' ); ?></span><?php }?>
					<p><span class="comment_author_link"><?php comment_author_link(); ?></span><span class="comment-date"><?php printf( __( '%1$s','karisma_text_domain' ), human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago',  get_comment_time() );?></span></p>
					<?php if ($comment->comment_approved == '0') : ?><p><em><?php _e( 'Your comment is awaiting moderation.','karisma_text_domain'); ?></em></p><?php endif;
					comment_text(); ?>
					<span class="comment-reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __( '<span class="fa fa-repeat"></span> Reply','karisma_text_domain' )))); ?></span><?php edit_comment_link( __( 'Edit','karisma_text_domain' ), '<span class="edit_comment"><span class="glyphicon glyphicon-edit"></span> ', '</span>' ); ?>
				</div><!-- .comment-data -->
			</div><!-- .commentwrapper -->
			<?php }
			endif;

/* 
* Default comment form wordpress
* Add add_action( 'do_krs_comment_template', 'krs_comments_template' ); in init.php 
*/
if ( !function_exists('krs_comments_template') ) {
	function krs_comments_template() { 
		if ( ot_get_option('krs_def_com_activated') == 'yes' ) :
			comments_template( '', true );
		endif;
	} 	
}

/* 
* Default facebook comment form wordpress
* Add add_action( 'do_krs_comment_facebook', 'krs_comments_facebook' ); in init.php 
*/
if ( !function_exists('krs_comments_facebook') ) {
	function krs_comments_facebook() { 
		if ( ot_get_option('krs_facebook_com_activated') == 'yes' ) : 
			echo '<div id="fb-root"></div>';
		echo '<div class="fb-comments" data-colorscheme="light" data-href="' . get_permalink() . '" data-width="100%"></div>';  
		endif;
	} 	
}