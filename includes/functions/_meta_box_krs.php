<?php 
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/* META BOX ROOM CUSTOM FIELD */

add_filter( 'rwmb_meta_boxes', 'room_info_meta_boxes' );
add_filter( 'rwmb_meta_boxes', 'page_meta_boxes' );
add_filter('rwmb_meta_boxes', 'contact_meta_box' );
add_filter('rwmb_meta_boxes', 'gallery_home_meta_box' );

function room_info_meta_boxes( $meta_boxes ) {
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_room',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __( 'Room photo gallery', 'textdomain' ),
        'post_types' => 'rooms',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __( 'Upload Gallery Photos (Recommended)', 'your-prefix' ),
                'id'               => "{$prefix}imgadv",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 6,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
                ),
            ),);
    $meta_boxes[] = array(
        'id'         => 'info_room',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __( 'Information Room', 'textdomain' ),
        'post_types' => 'rooms',
        'fields'     => array(
            array(
                'id'   => 'room_size',
                'name' => __( 'Room Size', 'textdomain' ),
                'type' => 'number',
                ),
            // SELECT BOX
            array(
                'name'        => __( 'Have Balcony ?', 'your-prefix' ),
                'id'          => "{$prefix}room_balcony",
                'type'        => 'select',
                'options'     => array(
                    'with Balcony' => __( 'with Balcony', 'your-prefix' ),
                    'without Balcony' => __( 'without Balcony', 'your-prefix' ),
                    ),
                'multiple'    => false,
                'placeholder' => __( 'Select an Item', 'your-prefix' ),
                ),
            array(
                'id'      => 'room_view',
                'name'    => __( 'View', 'textdomain' ),
                'type'    => 'text',
                ),
            array(
                'id'      => 'room_occupancy',
                'name'    => __( 'occupancy', 'textdomain' ),
                'type'    => 'number',
                ),
            array(
                'id'   => 'bed_size',
                'name' => __( 'Bed Size', 'textdomain' ),
                'type' => 'textarea',
                ),
            ),
        );
    $meta_boxes[] = array(
        'title' => __( 'Room Features', 'your-prefix' ),
        'post_types' => 'rooms',
        'fields' => array(
            // HEADING
            array(
                'type' => 'heading',
                'name' => __( 'Heading', 'your-prefix' ),
                'desc' => __( 'Optional description for this heading', 'your-prefix' ),
                ),
            array(
                'id'      => 'room_convenience',
                'name'    => __( 'Your Convenience', 'textdomain' ),
                'type'    => 'text',
                'clone' => true,
                'sort_clone' => true,
                ),
            array(
                'id'      => 'room_indulgence',
                'name'    => __( 'Your Indulgence', 'textdomain' ),
                'type'    => 'text',
                'clone'   => true,
                'sort_clone' => true,
                ),
            array(
                'id'      => 'room_comfort',
                'name'    => __( 'Your Comfort', 'textdomain' ),
                'type'    => 'text',
                'clone'   => true,
                'sort_clone' => true,
                ),
            ),
        );
    return $meta_boxes;
}
function page_meta_boxes( $meta_boxes ) {
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_page',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __( 'Room photo gallery', 'textdomain' ),
        'post_types' => 'page',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __( 'Upload Gallery Photos (Recommended)', 'your-prefix' ),
                'id'               => "{$prefix}imgpgaes",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 30,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
                ),
            ),);
    $meta_boxes[] = array(
        'id'         => 'gallery_page',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __( 'Room photo gallery', 'textdomain' ),
        'post_types' => 'page',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __( 'Upload Gallery Photos (Recommended)', 'your-prefix' ),
                'id'               => "{$prefix}imgpgaes",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 30,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
                ),
            ),);

    return $meta_boxes;
}

function contact_meta_box ($meta_boxes) {
    $meta_boxes[] = array(
        'title'  => __( 'Google Map', 'your-prefix' ),
        'post_types' => 'page',
        'fields' => array(
            // Map requires at least one address field (with type = text)
            array(
                'id'   => 'address',
                'name' => __( 'Address', 'your-prefix' ),
                'type' => 'text',
                'std'  => __( 'Hanoi, Vietnam', 'your-prefix' ),
                ),
            array(
                'id'            => 'map',
                'name'          => __( 'Location', 'your-prefix' ),
                'type'          => 'map',
                // Default location: 'latitude,longitude[,zoom]' (zoom is optional)
                'std'           => '-7.7815663,110.3385034,15',
                // Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
                'address_field' => 'address',
                'api_key'       => 'AIzaSyBRhisp4RwFkhIOOr4-Npe9wYf4lFrQO80', // https://metabox.io/docs/define-fields/#section-map
                ),
            ),
        );
    $meta_boxes[] = array(
        'title'  => __( 'Contact Details', 'your-prefix' ),
        'post_types' => 'page',
        'fields' => array(
            array(
                'id'      => 'contact_title',
                'name'    => __( 'Headline Contact', 'textdomain' ),
                'type'    => 'text',
                ),
            array(
                'id'      => 'contact_address',
                'name'    => __( 'Your Address', 'textdomain' ),
                'type'    => 'textarea',
                ),
            array(
                'id'      => 'contact_phone',
                'name'    => __( 'Phone Number', 'textdomain' ),
                'type'    => 'text',
                'clone'   => true,
                ),
             array(
                'id'      => 'contact_mobile',
                'name'    => __( 'Mobile Number', 'textdomain' ),
                'type'    => 'number',
                'clone'   => true,
                ),
             array(
                'id'      => 'contact_fax',
                'name'    => __( 'Fax Number', 'textdomain' ),
                'type'    => 'text',
                'clone'   => true,
                ),
             array(
                'id'      => 'contact_email',
                'name'    => __( 'Email', 'textdomain' ),
                'type'    => 'email',
                'clone'   => true,
                ),
            ),
        );
    return $meta_boxes;
}
function gallery_home_meta_box ($meta_boxes) {
    $meta_boxes[] = array(
        'title'  => __( 'Home Gallery Details', 'your-prefix' ),
        'post_types' => 'gallery',
        'fields' => array(
            array(
                'id'      => 'gallery_openning',
                'name'    => __( 'Opening Hour', 'textdomain' ),
                'type'    => 'time',
                ),
            array(
                'id'      => 'gallery_closing',
                'name'    => __( 'Closing Hour', 'textdomain' ),
                'type'    => 'time',
                ),
            array(
                'id'      => 'gallery_telephone',
                'name'    => __( 'Gallery Telephone', 'textdomain' ),
                'type'    => 'number',
                ),
            ),
        );
    return $meta_boxes;
}