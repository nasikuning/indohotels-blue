<?php 
function form_booking() { ?>
<form action="//demo.jogjahotels.id/referrer/booking/" method="GET" role="form" onSubmit="return validateBook()">
	<div class="booking col-md-12">

		<div class="row">
			<div class="booking-box-below col-xs-6 col-md-2">
				<div class="booking-title">Check-in</div>
				<div class="form-group">
					<div class='date' id='datetimepicker2'>
						<span readonly='true' id="from_date_day" style="display:none"><?php echo gmdate('l',time()+25200); ?></span>
						<input readonly='true' id="from_date" type="text" class="from_date from_date input-sm form-control" name="start" value="<?php echo gmdate('d-m-Y',time()+25200); ?>" />
						<input readonly='true' id="mobile_date" style="display:none" type="text"/>
					</div>
				</div>	
			</div>
			<div class="booking-box-below col-xs-6 col-md-2">
				<div class="booking-title">Check-out</div>
				<div class="form-group">
					<div class='date' id='datetimepicker2'>
						<span readonly='true' id="to_date_day" style="display:none"><?php echo gmdate('l',time()+25200+(60*60*24)); ?></span>
						<input readonly='true' type="text" id="to_date" class="to_date to_date_mobile input-sm form-control" name="end" value="<?php echo gmdate('d-m-Y',time()+25200+(60*60*24));?>" />
					</div>
				</div>	
			</div>
			<div class="booking-box-below col-xs-12 col-md-2">
				<div class="booking-title">Total Night(s)</div>
				<div class="form-group">
					<?php $night = 1; ?>
					<select id="t_night" class="t_night qty form-control" value="<?php echo $night;?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="night">
						<?php for ($i=1; $i <=30 ; $i++) { 
							echo '<option value="'. $i .'">'. $i .'</option>';
						} ?>
					</select>	
				</div>	
			</div>

			<div class="booking-box-below col-xs-4 col-md-1">
				<div class="booking-title">Room(s)</div>
				<select id="rooms" name="rooms" value="1" class="qty form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					<?php for ($i=1; $i <=6 ; $i++) { 
						echo '<option value="'. $i .'">'. $i .'</option>';
					} ?>
				</select>	
			</div>
			<div class="booking-box-below col-xs-4 col-md-1">
				<div class="booking-title">Adult(s)</div>
				<select id="adults" name="adults" value="2" class="qty form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					<option value="1">1</option>
					<option selected value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>			
			</div>
			<div class="booking-box-below col-xs-4 col-md-1">
				<div class="booking-title">Child(ren)</div>
				<select id="children" name="children" value="1" class="qty form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					<?php for ($i=1; $i <=6 ; $i++) { 
						echo '<option value="'. $i .'">'. $i .'</option>';
					} ?>
				</select>			
			</div>

			<input type="hidden" name="refferer" value="hotel.indohotels.id">
			<div class="button-check col-xs-12 col-md-3">
				<button type="submit" class="btn btn-check">Check Availability
				</button>
			</div>

		</div> <!-- end row -->
		<div class="powered-by">
			<img src="https://media.jogjahotels.id/website/powered-by-jogjahotels.svg">
		</div>
	</div> <!-- end col-md-12 -->
</form>

<?php }