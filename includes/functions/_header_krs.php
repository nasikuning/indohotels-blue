<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }


function krs_headscript()  {
	echo ot_get_option("krs_head") . "\n";  
}
if ( !function_exists('krs_header') ) {
function krs_header() {
	if(ot_get_option('krs_favicon') != '') {
		$krs_metadata_arg[] = '<link rel="shortcut icon" href="'. ot_get_option('krs_favicon') .' " />';
		}
	if(ot_get_option('krs_appleicon') != '') {
		$krs_metadata_arg[] = '<link rel="apple-touch-icon-precomposed" href="'. ot_get_option('krs_favicon') .' " />';
		}
		$krs_metadata_arg[] = '<link rel="profile" href="http://gmpg.org/xfn/11" />';
		$krs_metadata_arg[] = '<link rel="pingback" href="' . get_bloginfo('pingback_url') . '" />';
		$krs_metadata_arg[] = '<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">';
	if (ot_get_option('krs_gplus_sn') != '') { 
		$krs_metadata_arg[] = '<link rel="author" href="' . ot_get_option('krs_gplus_sn') . '" />'; 
		}
	if ( ot_get_option('krs_facebook_com_activated') == 'yes' ) {
		echo "<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8\";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>";
	}
	echo implode("\n", $krs_metadata_arg);
	} 
}