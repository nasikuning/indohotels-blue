<?php
/*
 *  Author: Amri Karisma | @amrikarisma
 *  URL: kentos.org | @amrikarisma
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
    \*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Theme Support
    \*------------------------------------*/

    if (!isset($content_width))
    {
        $content_width = 900;
    }

    if (function_exists('add_theme_support'))
    {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 320, 200, true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size( 'admin-list-thumb', 80, 80, true); //admin thumbnail preview
    add_image_size( 'album-grid', 450, 450, true );
    add_image_size('gallery-slide', 900, 500, true);
    add_image_size('gallery-slide-full', 1920, 1080, true);
    add_image_size('custom-size', 900, 300, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
    'default-color' => 'FFF',
    'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
    'default-image'         => get_template_directory_uri() . '/img/headers/default.jpg',
    'header-text'           => false,
    'default-text-color'        => '000',
    'width'             => 1000,
    'height'            => 198,
    'random-default'        => false,
    'wp-head-callback'      => $wphead_cb,
    'admin-head-callback'       => $adminhead_cb,
    'admin-preview-callback'    => $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add custom logo themes
    /*function theme_prefix_setup() {

    add_theme_support( 'custom-logo', array(
        'height'      => 100,
        'width'       => 400,
        'flex-width' => true,
    ) );

}
    add_action( 'after_setup_theme', 'theme_prefix_setup' );
*/
        // Localisation Support
    load_theme_textdomain('karisma_text_domain', krs_style . 'vendor/languages');
}

/*------------------------------------*\
    Functions
    \*------------------------------------*/

// karisma navigation
    function karisma_nav()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'header-menu',
                'menu'            => 'header-menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
                )
            );
    }
    function karisma_nav_footer()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'footer-menu',
                'menu'            => 'footer-menu',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul class="footer-menu">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
                )
            );
    }
    function special_nav_class ($classes, $item) {
        if (in_array('current-menu-item', $classes) ){
            $classes[] = 'active ';
        }
        return $classes;
    }

// Register karisma Navigation
function register_karisma_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'karisma_text_domain'), // Main Navigation
        'footer-menu' => __('Footer Menu', 'karisma_text_domain') // Extra Navigation if needed (duplicate as many as you need!)
        ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'karisma_text_domain'),
        'description' => __('Description for this widget-area...', 'karisma_text_domain'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
        ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
        ));
}
// Add class for fist post


// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
// Add it to a column in WP-Admin - (Optional)
function posts_column_views($defaults){
    $defaults['post_views'] = __('Views');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
    if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}
// Add class before iframe
function wrap_embed_with_div($html, $url, $attr) {
    if( stripos( $html, 'youtube.com' ) !== FALSE && stripos( $html, 'iframe' ) !== FALSE )
           $html = str_replace( '<iframe', '<iframe class="embed-responsive-item"  ', $html );
        $html = '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
     return $html;
}

 add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);

 function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );
function wpdocs_excerpt_more( $more ) {
    return ' <a href="'.get_the_permalink().'" rel="nofollow">Read More...</a>';
}



add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function karisma_pagination() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="Page navigation"><ul class="pagination">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li></li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li></li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul></div>' . "\n";

}
//Repalace avatar to gravatar-img
function krs_avatar_css($class) {
    $class = str_replace("class='avatar", "class='gravatar-img", $class) ;
    return $class;
}

//Get avatar URL

function krs_get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function karisma_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}
function get_resolution_images($reso=''){
    $optional_size = '';
    global $post;
    if(!empty($post->ID)) {
        $img_id = get_post_thumbnail_id($post->ID);
        $image = wp_get_attachment_image_src($img_id, $optional_size);
        $reso =  $image[1]. 'x' . $image[2];
    }
    return $reso;
}
add_action('wp', 'get_resolution_images');
// Custom Gravatar in Settings > Discussion
function karismagravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

//featured image
function autoset_featured() {
          global $post;
          $already_has_thumb = has_post_thumbnail($post->ID);
              if (!$already_has_thumb)  {
              $attached_image = get_children( "post_parent=$post->ID&post_type=attachment&post_mime_type=image&numberposts=1" );
                          if ($attached_image) {
                                foreach ($attached_image as $attachment_id => $attachment) {
                                set_post_thumbnail($post->ID, $attachment_id);
                                }
                           }
                        }
      }
add_action('the_post', 'autoset_featured');
add_action('save_post', 'autoset_featured');
add_action('draft_to_publish', 'autoset_featured');
add_action('new_to_publish', 'autoset_featured');
add_action('pending_to_publish', 'autoset_featured');
add_action('future_to_publish', 'autoset_featured');


/*
* Get Info Themes
*/
function krs_let_to_num( $size ) {
    $let = substr( $size, -1 );
    $ret = substr( $size, 0, -1 );
    switch( strtoupper( $let ) ) {
        case 'P':
        $ret *= 1024;
        case 'T':
        $ret *= 1024;
        case 'G':
        $ret *= 1024;
        case 'M':
        $ret *= 1024;
        case 'K':
        $ret *= 1024;
    }
    return $ret;
}
function krs_themes_info() {
    if( is_admin() ) {
        $karisma_theme = wp_get_theme();
        $krsinfotheme = '<div class="format-setting-wrap"><div class="format-setting-label"><h3 class="label"><strong>System Information</strong></h3></div>';
        $krsinfotheme .= '<ul>';
        $krsinfotheme .= '<li><strong>Theme Name:</strong> ' . $karisma_theme->Name . '</li>';
        $krsinfotheme .= '<li><strong>Theme Version:</strong> ' . $karisma_theme->Version . '</li>';
        $krsinfotheme .= '<li><strong>Author:</strong> ' . $karisma_theme->get( 'ThemeURI' ) . '</li>';
        $krsinfotheme .= '<li><strong>Home URL:</strong>' . home_url() . '</li>';
        $krsinfotheme .= '<li><strong>Site URL:</strong>' . site_url() . '</li>';
        if ( is_multisite() ) {
            $krsinfotheme .= '<li><strong>WordPress Version:</strong>' . 'WPMU ' . get_bloginfo('version') . '</li>';
        } else {
            $krsinfotheme .= '<li><strong>WordPress Version:</strong>'. 'WP ' . get_bloginfo('version') . '</li>';
        }
        if ( function_exists( 'phpversion' ) ) {
            $krsinfotheme .= '<li><strong>PHP Version:</strong>' . esc_html( phpversion() ) . '</li>';
        }
        if (function_exists( 'size_format' )) {
            $krsinfotheme .= '<li><strong>Memory Limit:</strong>';
            $mem_limit = krs_let_to_num( WP_MEMORY_LIMIT );
            if ( $mem_limit < 67108864 ) {
                $krsinfotheme .= '<mark class="error">' . size_format( $mem_limit ) .' - Recommended memory to at least 64MB. Please see: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a></mark>';
            } else {
                $krsinfotheme .= '<mark class="yes">' . size_format( $mem_limit ) . '</mark>';
            }
            $krsinfotheme .= '</li>';
            $krsinfotheme .= '<li><strong>WP Max Upload Size:</strong>'. size_format( wp_max_upload_size() ) .' - Recommended is 2MB (Find tutorial about it in <a href="https://www.google.com/#q=WP+Max+Upload+Size" target="_blank">Google</a>)</li>';
        }
        if ( function_exists( 'ini_get' ) ) {
            $krsinfotheme .= '<li><strong>PHP Time Limit:</strong>'. ini_get('max_execution_time') .'</li>';
        }
        if ( defined('WP_DEBUG') && WP_DEBUG ) {
            $krsinfotheme .= '<li><strong>WP Debug Mode:</strong><mark class="yes"><b>Yes</b> - If life website please turn off WP debug mode. Please see: <a href="http://codex.wordpress.org/Debugging_in_WordPress" target="_blank">Debugging in WordPress</a></mark></mark></li>';
        } else {
            $krsinfotheme .= '<li><strong>WP Debug Mode:</strong><mark class="no">No</mark></li>';
        }
        $krsinfotheme .= '</ul></div>';

        return $krsinfotheme;
    }
}
function krs_license() {
    if( is_admin() ) {
        $karisma_theme = wp_get_theme();
        $krsinfotheme = '<div class="format-setting-wrap"><div class="format-setting-label"><h3 class="label"><strong>Theme License</strong></h3></div>';
        $krsinfotheme .= '<ol>';
        $krsinfotheme .= '<li>The PHP code is licensed under the GPL license as is WordPress itself. <a href="http://codex.wordpress.org/GPL/">http://codex.wordpress.org/GPL/</a></li>';
        $krsinfotheme .= '<li>All other parts of the theme including, but not limited to the CSS code, images, and design are licensed according to the license purchased.<a href="https://karismaid.com/license/">https://karismaid.com/license/</a></li>';
        $krsinfotheme .= '<li>This is single unlimited domain for own domain only <strong>cannot resale theme or install for other people</strong>. Require: place the single license in theme folder.</li>';
        $krsinfotheme .= '<li>Intellectual license from <a href="http://karismaid.com">karismaid.com</a></li>';

        $krsinfotheme .= '</ol></div>';

        return $krsinfotheme;
    }
}


?>
