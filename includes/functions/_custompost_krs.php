<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Remove the slug from published post permalinks. Only affect our custom post type, though.
 */
function krs_remove_cpt_slug( $post_link, $post, $leavename ) {

    if ( 'hotel-info' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}


/**
 * Have WordPress match postname to any of our public post types (post, page, race)
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * By default, core only accounts for posts and pages where the slug is /post-name/
 */
function krs_parse_request_trick( $query ) {

    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'page', 'hotel-info' ) );
    }
}
/*------------------------------------*\
	Custom Post Types
    \*------------------------------------*/
    function hotel_info()
    {
    register_post_type('hotel-info', // Register Custom Post Type
        array(
            'labels' => array(
            'name' => __('Hotel Information', 'indohotels'), // Rename these to suit
            'singular_name' => __('Hotel Information', 'indohotels'),
            'add_new' => __('Add New', 'indohotels'),
            'add_new_item' => __('Add New Hotel Information Post', 'indohotels'),
            'edit' => __('Edit', 'indohotels'),
            'edit_item' => __('Edit Hotel Information Post', 'indohotels'),
            'new_item' => __('New Hotel Information Post', 'indohotels'),
            'view' => __('View Hotel Information Post', 'indohotels'),
            'view_item' => __('View Hotel Information Post', 'indohotels'),
            'search_items' => __('Search Hotel Information Post', 'indohotels'),
            'not_found' => __('No Hotel Information found', 'indohotels'),
            'not_found_in_trash' => __('No Hotel Information Posts found in Trash', 'indohotels')
            ),
            'menu_icon'   => 'dashicons-info',
            'publicly_queryable' => true,
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'menu_position' => 5,
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => false,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            ) // Add Category and Post Tags support
            ));
}
function rooms_post()
{
    register_taxonomy_for_object_type('category', 'rooms'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'rooms');
    register_post_type('rooms', // Register Custom Post Type
        array(
            'labels' => array(
            'name' => __('Rooms', 'indohotels'), // Rename these to suit
            'singular_name' => __('Rooms', 'indohotels'),
            'add_new' => __('Add New', 'indohotels'),
            'add_new_item' => __('Add New Rooms Post', 'indohotels'),
            'edit' => __('Edit', 'indohotels'),
            'edit_item' => __('Edit Rooms Post', 'indohotels'),
            'new_item' => __('New Rooms Post', 'indohotels'),
            'view' => __('View Rooms Post', 'indohotels'),
            'view_item' => __('View Rooms Post', 'indohotels'),
            'search_items' => __('Search Rooms Post', 'indohotels'),
            'not_found' => __('No Rooms found', 'indohotels'),
            'not_found_in_trash' => __('No Rooms Posts found in Trash', 'indohotels')
            ),
            'menu_icon'   => 'dashicons-admin-home',
            'publicly_queryable' => true,
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            ) // Add Category and Post Tags support
            ));
}

function deals_post()
{
    register_taxonomy_for_object_type('category', 'deals'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'deals');
    register_post_type('deals', // Register Custom Post Type
        array(
            'labels' => array(
            'name' => __('Deals', 'indohotels'), // Rename these to suit
            'singular_name' => __('Deals', 'indohotels'),
            'add_new' => __('Add New', 'indohotels'),
            'add_new_item' => __('Add New Deals Post', 'indohotels'),
            'edit' => __('Edit', 'indohotels'),
            'edit_item' => __('Edit Deals Post', 'indohotels'),
            'new_item' => __('New Deals Post', 'indohotels'),
            'view' => __('View Deals Post', 'indohotels'),
            'view_item' => __('View Deals Post', 'indohotels'),
            'search_items' => __('Search Deals Post', 'indohotels'),
            'not_found' => __('No Deals found', 'indohotels'),
            'not_found_in_trash' => __('No Deals Posts found in Trash', 'indohotels')
            ),
            'menu_icon'   => 'dashicons-clipboard',
            'menu_position'       => 3,
            'query_var' => true,
            'capability_type' => 'post',

            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => false,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            ));
}