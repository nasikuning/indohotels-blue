<?php //
?>
<style type="text/css">
	body {
		font-family: <?php echo $fontsColor['font-family']; ?>;
		color: <?php echo $fontsColor['font-color']; ?>;

	}
	.navbar-fixed-top,.nav ul.dropdown-menu,.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline,
	.btn-check, .btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}
	h1.title,h2.room-title,
	.btn-outline:hover, .btn-outline:focus, .btn-outline:active, .btn-outline.active,
	.book-room:hover,.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left, .owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $mainColor; ?>;
	}
	::selection {
		background: <?php echo $mainColor; ?>;
	}
	button, html input[type=button], input[type=reset], input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}
	.booking .form-control, .booking .form-control[readonly], .booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;

	}
	.box-gallery {
		border-top: 2px solid <?php echo $mainColor; ?>;
		border-bottom: 2px solid <?php echo $mainColor; ?>;
	}

	/* Secoundary Color */
	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}
	/* Background Image */



	/* Font Color */
	.booking .booking-title,
	#ros-in h2,
	span.room-value,
	h3.room-details-f-title {
		color: <?php echo $mainColor; ?>;
	}
	/* 3nd Color */
	footer .nav-bottom, nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}
</style>