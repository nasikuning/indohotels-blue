// Animated css
jQuery(document).ready(function() {
	jQuery('.box-logo').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.welcome-desc').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated bounceInLeft',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.welcome-title').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn', // Class to add to the elements when they are visible
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.feature-description').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.btn-social .fa').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated bounceIn', // Class to add to the elements when they are visible
	    offset: 10,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.logo-wrap').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated swing',
	    offset: 100,
	    repeat: true,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});

jQuery(document).ready(function() {
	jQuery('li.product').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated fadeInLeft',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.button').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated tada',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
