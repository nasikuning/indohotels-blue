<?php /* Template Name: Contact Template */ get_header('image'); ?>


<main role="main" class="col-md-12">
	<!-- section -->
	<section class="container">


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<h1 class="title text-center"><?php the_title(); ?></h1>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<br class="clear">
				<div class="contact-box col-md-12">
					<div class="contact-headline">
						<h3><?php echo rwmb_meta('contact_title'); ?></h3>
					</div>
					<div class="contact-address">
						<?php echo rwmb_meta('contact_address'); ?>
					</div>
					<div class="contact-info">
						<table>
							<tbody>
								<tr>
									<td><i class="fa fa-phone" aria-hidden="true"></i></td>
									<td>Phone</td><td>:</td>
									<td>
										<ul>								
											<?php 
											$values = rwmb_meta( 'contact_phone' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</td>
								</tr>
								<tr>
									<td><i class="fa fa-mobile" aria-hidden="true"></i></td>
									<td>Mobile</td><td>:</td>
									<td>
										<ul>									
											<?php 
											$values = rwmb_meta( 'contact_mobile' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</td>
								</tr>
								<tr>
									<td><i class="fa fa-fax" aria-hidden="true"></i></td>
									<td>Fax</td><td>:</td>
									<td>
										<ul>								
											<?php 
											$values = rwmb_meta( 'contact_fax' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>							
									</td>	
								</tr>
								<tr>
									<td><i class="fa fa-envelope" aria-hidden="true"></i></td>
									<td>Email</td><td>:</td>
									<td>
										<ul>			
											<?php 
											$values = rwmb_meta( 'contact_email' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>	
									</td>
								</tr>
							</tbody>
						</table>


					</div>
				</div>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h2 class="title text-center"><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h2>

		</article>
		<!-- /article -->

	<?php endif; ?>

</section>
<!-- /section -->
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
