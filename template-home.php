<?php /* Template Name: Home Template */ get_header('home'); ?>
<main role="main">
	<section>
		<div class="container">
			<div class="row">
				<?php form_booking(); ?>
			</div>
		</div>
	</section>
	<!-- section -->

	<section id="ros-in" style="background-image: url(<?php echo krs_url; ?>/asset/images/breather-187924.jpg); background-attachment: fixed; background-size: cover;">
		<div class="container">
			<div class="box-bg">
				<h2><?php echo ot_get_option('krs_ros_in_title');?></h2>
				<?php echo ot_get_option('krs_ros_in');?>
			</div>
		</div>
	</section>
	<!-- /section -->

  <!-- section -->
	<section id="room-section" style="background-image: linear-gradient(rgba(235, 235, 235, 0.5), rgba(235, 235, 235, 0.5)), url(<?php echo ot_get_option('krs_background_text1'); ?>);" class="padtb-large">

			<div class="container">

        <?php
				$args = array(
          'post_type'=>'rooms',
          'posts_per_page' => 6,
        );
        //query_posts($args);
        $krs_query = new WP_Query( $args );

        $count = $krs_query->post_count;

        if(($count == 2) || ($count == 4)) {
            $col = 'col-md-6';
        } else {
            $col = 'col-md-4';
        }

        if ($krs_query->have_posts()): ?>
        <div class="box-bg" <?php if($col == 'col-md-4') { echo 'style="width: 70%; margin: 0 auto";'; } ?>>
        	<h2>Our Rooms</h2>
            <div class="row">
                <?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
                <div class="<?php echo $col ?>">

                    <div class="box-room">
                        <div class="thumb">
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
                            </a>
                            <?php endif; ?>
                        </div>
                        <h4><?php the_title(); ?></h4>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
            <?php endif; ?>

        </div> <!-- /box-bg -->
      </div>
  </section>
	<!-- /section -->

	<!-- gallery -->
	<section class="padtb-large">
		<div class="container">
			<div class="box-home-gallery">
				<h2>Our Facilities</h2>
				<div class="home-carousel owl-carousel home-gallery">
					<?php
					$args = array(
						'post_type' => 'gallery',
						'phototype'  => 'home',
						);
					query_posts($args);
					if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="item thumb">
						<div class="thumbnails">
							<!-- post thumbnail -->
							<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
								<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
							<?php endif; ?>
							<!-- /post thumbnail -->
						</div><!-- end .thumbnails -->
						<!-- post title -->
						<div class="title-gallery-home">
							<h2><?php echo get_the_title(); ?></h2>
							<div class="gallery-time">Opening hours	: <span><?php echo rwmb_meta('gallery_openning'); ?></span> - <span><?php echo rwmb_meta('gallery_closing'); ?></span></div>
							<div class="gallery-telephone">
								<div>Phone		: <span><?php echo rwmb_meta('gallery_telephone'); ?></span></div>
							</div>
						</div>
					<!-- /post title -->
					</div>
					<?php endwhile; ?>
					<?php endif; ?>

				</div>
			</div><!-- end .box-home-gallery -->
		</div><!-- end .container -->
	</section>

<!-- end gallery -->
<section class="container">
	<div class="box-home-gallery">
		<div class="home-carousel owl-carousel home-text-slide">
			<?php
			$args = array(
				'post_type' => 'gallery',
				'phototype'  => 'home',
				);
			query_posts($args);
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="item thumb">
				<div class="thumbnails">
					<!-- post thumbnail -->
					<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
						<?php //the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
					<?php endif; ?>
					<!-- /post thumbnail -->
				</div>
				<!-- post title -->
				<div class="title-gallery-home">
					<h2><?php echo get_the_title(); ?></h2>
					<div class="gallery-time">Opening hours	:
						<span><?php echo rwmb_meta('gallery_openning'); ?></span> -
						<span><?php echo rwmb_meta('gallery_closing'); ?></span>
					</div>
					<div class="gallery-telephone">
						<div>Phone : <span><?php echo rwmb_meta('gallery_telephone'); ?></span></div>
					</div><!-- end .gallery-time -->
				</div><!-- end .title-gallery-home -->
			<!-- /post title -->
			</div><!-- end .item -->
			<?php endwhile; ?>
			<?php endif; ?>

		</div><!-- end .home-carousel -->
	</div><!-- end .box-home-gallery -->
</section>

</main>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.home-gallery').owlCarousel({
			margin:18,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			loop:true,
			nav:true,
			navText: ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'>"],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					margin:5,
				},
				600:{
					items:3,
					margin:8,

				},
				1024:{
					items:4,
					margin:14,
				}
			}
		});
		/* Testimonial */
		jQuery('.home-text-slide').owlCarousel({
			margin:18,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			loop:true,
			nav:true,
			navText: ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'>"],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					margin:5,
				},
				600:{
					items:1,
					margin:8,
				},
				1024:{
					items:1,
					margin:14,
				}
			}
		});
	});
</script>
<?php get_footer(); ?>
